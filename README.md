# Prerequisite
1. Install java 8 or 11 in the system and set the environment variables.
2. Install maven in the system and set the environment variables.
3. Install IntelliJ IDEA

# Project Setup
1. Download project from git repository.(https://gitlab.com/jignesh.shah3086/plivo-sdk.git)
2. Open IntelliJ IDEA Tool.
3. Click on File.
4. Click New --> Project from Existing Sources...
5. Select Project from the directory where it is downloaded.

# Steps To Execute Code
1. Open Terminal (In windows: Alt+12)
2. To execute code insert command - mvn test
3. If you are facing any error then execute commands in below sequence:  
 a. mvn clean  
 b. mvn install  
 c. mvn test

# Execution Report
Inside project --> target --> sufire-reports --> index.html
