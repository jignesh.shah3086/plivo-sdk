package autoweb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class Verify {


    WebDriver driver;
    Element element;
    testng_logs logs = new testng_logs();


    public Verify(WebDriver dri) {
        this.driver = dri;
        element = new Element(driver);

    }

    public void element_is_present(String element_name) {
        boolean bool = false;
        logs.test_step("verify " + element_name + " is displayed");
        try {
            if (element.find(element_name).isDisplayed()) {
                bool = true;
                logs.test_step("Test Passed");
            }
        } catch (Exception e) {

        }

        assertThat(bool).isTrue();

    }

    public boolean check_element_is_present(String element_name) {
        boolean bool = false;

        try {
            if (element.find(element_name).isDisplayed()) {
                bool = true;

            }
        } catch (Exception e) {

        }

        return bool;

    }

    public void element_is_enable(String element_name) {
        logs.test_step("verify " + element_name + " is enable");
        boolean bool = false;
        try {
            if (element.find(element_name).isEnabled()) {
                bool = true;
                logs.test_step("Test Passed");
            }
        } catch (Exception e) {

        }
        assertThat(bool).isTrue();
    }

    public void element_is_selected(String element_name) {
        logs.test_step("verify " + element_name + " is selected");
        boolean bool = false;
        try {
            if (element.find(element_name).isSelected()) {
                bool = true;
                logs.test_step("Test Passed");
            }
        } catch (Exception e) {

        }
        assertThat(bool).isTrue();

    }

    public void current_title_is_equal_to(String title) {
        logs.test_step("verify current page title is " + title);
        assertThat(driver.getTitle()).isEqualTo(title);
    }

    public void element_text_is_equal_to(String element_name, String text_to_verify) {
        logs.test_step("verify element " + element_name + " text is equal " + text_to_verify);
        assertThat(element.find(element_name).getText()).isEqualTo(text_to_verify);
    }




    public ArrayList<String> convert_web_element_to_string_list(List<WebElement> elements_list) {

        ArrayList<String> actual_list = new ArrayList<>();
        for (WebElement element_list_member : elements_list) {
            actual_list.add(element_list_member.getText());
        }
        return actual_list;
    }


}
