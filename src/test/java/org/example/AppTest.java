package org.example;


import autoweb.Session;
import autoweb.Wait;
import init.base;

import object_repository.calling_page_steps;
import object_repository.login_page_steps;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

/**
 * Unit test for simple App.
 */
public class AppTest extends base {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void CallingTest() {

        Session calle_session = new Session();
        WebDriver callee_session_driver = calle_session.create_new_session();
        login_page_steps caller_login_page = new login_page_steps(driver);
        calling_page_steps caller_calling_page = new calling_page_steps(driver);
        login_page_steps callee_login_page = new login_page_steps(callee_session_driver);
        calling_page_steps callee_calling_page = new calling_page_steps(callee_session_driver);
        Wait wait = new Wait(callee_session_driver);

        // Caller Logged In
        caller_login_page.perform_login("csdkcaller2205010941826351041747", "plivo");
        caller_calling_page.verify_calling_page_title_is_present_on_page();
        caller_calling_page.verify_call_status_text_is_equal_to("Idle");


        // Callee Logged in
        callee_login_page.perform_login("csdkcallee22953588125995310", "plivo");
        callee_calling_page.verify_calling_page_title_is_present_on_page();
        callee_calling_page.verify_call_status_text_is_equal_to("Idle");

        //Caller Enter Mobile Number and Call

        caller_calling_page.verify_phone_no_text_box_is_present_on_page();
        caller_calling_page.enter_text_at_phone_no_text_box("+911234567890");

        caller_calling_page.verify_call_button_is_present_on_page();
        caller_calling_page.click_on_call_button();


        //Verify Call status on Caller side

        caller_calling_page.verify_hang_up_button_is_present_on_page();

        caller_calling_page.verify_call_status_text_is_equal_to("Ringing...");

        //Verify Call Status on Callee Side

        callee_calling_page.verify_call_status_text_is_equal_to("Ringing...");
        callee_calling_page.verify_incoming_box_is_present_on_page();

        callee_calling_page.verify_call_answer_button_present_on_page();


        //call accepted by callee
        callee_calling_page.click_on_call_answer_button();
        callee_calling_page.verify_call_status_text_is_equal_to("Answered");
        wait.wait_for_second(10);

        //caller hang up the call
        caller_calling_page.click_on_hang_up_button();
        wait.wait_for_second(5);
        //verify call status on callee
        callee_calling_page.verify_call_status_text_is_equal_to("Idle");

        //verify call status on caller
        caller_calling_page.verify_call_status_text_is_equal_to("Idle");

        calle_session.close_session(callee_session_driver);

    }
}
