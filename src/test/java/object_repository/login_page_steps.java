package object_repository;

import autoweb.Element;
import autoweb.Verify;
import autoweb.Wait;
import org.openqa.selenium.WebDriver;

public class login_page_steps {

    WebDriver driver;
    Element element;
    Verify verify;
    Wait wait;

    public login_page_steps(WebDriver driver) {
        this.driver = driver;
        element = new Element(driver);
        verify = new Verify(driver);
        wait = new Wait(driver);
    }

    String login_user_id_text_box = "id:loginUser";
    String login_password_text_box = "id:loginPwd";
    String login_button = "id:clickLogin";
    String login_title = "xpath://h2[contains(.,'Log In to your Plivo account')]";


    public void enter_text_at_user_id(String text_to_enter) {
        element.enter_text(login_user_id_text_box, text_to_enter);
    }

    public void verify_login_user_id_is_present_on_page() {
        verify.element_is_present(login_user_id_text_box);
    }


    public void enter_text_at_login_password(String text_to_enter) {
        element.enter_text(login_password_text_box, text_to_enter);
    }

    public void verify_login_password_is_present_on_page() {
        verify.element_is_present(login_password_text_box);
    }

    public void click_on_login_button() {
        element.click(login_button);
    }

    public void verify_login_button_is_present_on_page() {
        verify.element_is_present(login_button);
    }

    public void verify_login_title_is_present_on_page() {
        element.click(login_title);
    }




    //============================
    public void perform_login(String user_name,String password)
    {
        verify_login_button_is_present_on_page();
        verify_login_password_is_present_on_page();
        verify_login_button_is_present_on_page();
        enter_text_at_user_id(user_name);
        enter_text_at_login_password(password);
        click_on_login_button();
        wait.wait_for_second(5);

    }

}
