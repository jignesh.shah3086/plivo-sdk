package object_repository;

import autoweb.Element;
import autoweb.Verify;
import autoweb.Wait;
import org.openqa.selenium.WebDriver;

public class calling_page_steps {

    WebDriver driver;
    Element element;
    Verify verify;
    Wait wait;

    public calling_page_steps(WebDriver driver) {
        this.driver = driver;
        element = new Element(driver);
        verify = new Verify(driver);
        wait = new Wait(driver);
    }


    String calling_page_title = "xpath://div[@id=\"callContainer\"]//figcaption[contains(text(),'Plivo Web Client')]";
    String phone_no_text_box = "id:toNumber";
    String call_button = "id:makecall";
    String hang_up_button = "xpath://button[contains(.,'HANGUP')]";
    String call_status_text = "id:callstatus";
    String incoming_box = "id:incomingCallDefault";
    String incoming_title = "id:boundType";
    String call_answer_button = "xpath://button[contains(.,'ANSWER')]";


    public void verify_calling_page_title_is_present_on_page() {
        verify.element_is_present(calling_page_title);
    }

    public void verify_phone_no_text_box_is_present_on_page() {
        verify.element_is_present(phone_no_text_box);
    }

    public void enter_text_at_phone_no_text_box(String text_to_enter) {
        element.enter_text(phone_no_text_box, text_to_enter);
    }

    public void verify_call_button_is_present_on_page() {
        verify.element_is_present(call_button);
    }

    public void click_on_call_button() {
        wait.wait_for_second(3);
        try {
            element.click_using_js(element.find(call_button));
        } catch (Exception e) {
            e.printStackTrace();
        }
        wait.wait_for_second(5);
    }

    public void verify_hang_up_button_is_present_on_page() {
        wait.wait_until_element_is_visible(hang_up_button);
        verify.element_is_present(hang_up_button);
    }

    public void click_on_hang_up_button() {
        element.click(hang_up_button);
    }

    public void verify_call_status_text_is_present_on_page() {
        verify.element_is_present(call_status_text);
    }

    public void verify_call_status_text_is_equal_to(String text_to_verify) {
        verify.element_text_is_equal_to(call_status_text, text_to_verify);
    }

    public void verify_incoming_box_is_present_on_page() {
        wait.wait_until_element_is_visible(incoming_box);
        verify.element_is_present(incoming_box);
    }

    public void verify_incoming_title_is_present_on_page() {
        verify.element_is_present(incoming_title);
    }

    public void verify_call_answer_button_present_on_page() {
        verify.element_is_present(call_answer_button);
    }

    public void click_on_call_answer_button() {
        element.click(call_answer_button);
        wait.wait_for_second(5);
    }


}
